package se.intem.web.taglib.combined.tags;

import jakarta.servlet.jsp.tagext.BodyTagSupport;

public abstract class SourceTagSupport extends BodyTagSupport {

    private String path;

    public void setPath(final String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

}
