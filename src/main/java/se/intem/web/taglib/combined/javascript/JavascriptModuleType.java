package se.intem.web.taglib.combined.javascript;

public enum JavascriptModuleType {
    // <script type="module" />
    module,
    // <script nomodule />
    nomodule
}
