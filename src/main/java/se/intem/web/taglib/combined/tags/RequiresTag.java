package se.intem.web.taglib.combined.tags;

import jakarta.servlet.jsp.JspException;
import se.intem.web.taglib.combined.tree.ConfigurationItem;

import java.util.UUID;

public class RequiresTag extends ConfigurationItemAwareTagSupport {

    private transient ConfigurationItem configurationItem = new ConfigurationItem();

    @Override
    public int doEndTag() throws JspException {

        if (configurationItem.hasDependencies()) {
            configurationItem.setName("requires-" + UUID.randomUUID().toString());
            configurationItem.setRoot(true);
            addConfigurationItem(configurationItem);
        }

        cleanup();
        return EVAL_PAGE;
    }

    /* Note: setters will be called BEFORE doStartTag, so cleanup must be done after tag is complete. */
    private void cleanup() {
        this.configurationItem = new ConfigurationItem();
    }

    public void setRequires(final String requires) {
        this.configurationItem.addRequires(requires);
    }

}
