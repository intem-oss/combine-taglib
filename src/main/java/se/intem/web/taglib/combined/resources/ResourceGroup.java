package se.intem.web.taglib.combined.resources;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import se.intem.web.taglib.combined.RequestPath;
import se.intem.web.taglib.combined.ResourceType;

import java.util.Iterator;
import java.util.List;

public class ResourceGroup implements Iterable<RequestPathBundle> {

    private List<RequestPathBundle<RequestPath>> js = Lists.newArrayList();
    private List<RequestPathBundle<RequestPath>> css = Lists.newArrayList();

    public void addBundle(final RequestPathBundle bundle) {
        if (ResourceType.js.equals(bundle.getType())) {
            js.add(bundle);
        } else {
            css.add(bundle);
        }
    }

    public List<RequestPathBundle<RequestPath>> getJs() {
        return js;
    }

    public List<RequestPathBundle<RequestPath>> getCss() {
        return css;
    }

    @Override
    public Iterator<RequestPathBundle> iterator() {
        return Iterators.concat(css.iterator(), js.iterator());
    }

    public Iterable<RequestPath> getJavascriptRequestPaths() {
        List<RequestPathBundle<RequestPath>> bundles = js;

        // https://codereview.stackexchange.com/a/222143
        Iterable<RequestPath> iterable = () -> bundles.stream()
                .flatMap(it -> it.getPaths().stream())
                .iterator();

        return iterable;
    }

    public Iterable<RequestPath> getCssRequestPaths() {
        return () -> css.stream()
                .flatMap(it -> it.getPaths().stream())
                .iterator();
    }

}
