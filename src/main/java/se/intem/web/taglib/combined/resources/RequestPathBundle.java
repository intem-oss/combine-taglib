package se.intem.web.taglib.combined.resources;

import se.intem.web.taglib.combined.RequestPath;
import se.intem.web.taglib.combined.ResourceType;

import java.util.List;

public interface RequestPathBundle<T extends RequestPath> {
    List<T> getPaths();

    ResourceType getType();
}
