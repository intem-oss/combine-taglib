package se.intem.web.taglib.combined.tags;

import com.google.common.base.Ascii;
import org.slf4j.Logger;
import se.intem.web.taglib.combined.configuration.InlineContent;

import static org.slf4j.LoggerFactory.*;

public class InlineStyleEarlyTag extends InlineTagSupport {

    /** Logger for this class */
    private static final Logger log = getLogger(InlineStyleEarlyTag.class);

    @Override
    protected void addContents(final InlineContent contents) {

        if (hasLayoutBeenCalled()) {

            String truncated = Ascii.truncate(contents.getContents(), 200, "...");
            log.warn("Layout has already been called for contents {}", truncated);

            throw new IllegalStateException("Adding inline style, but layout has already been called. "
                    + "All configuration must be completed before any layout tag is called.");
        }

        getConfigurationItems().addInlineStyleEarly(contents);
    }

}
