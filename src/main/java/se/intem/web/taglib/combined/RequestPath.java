package se.intem.web.taglib.combined;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import se.intem.web.taglib.combined.javascript.JavascriptModuleType;

import java.util.Map;
import java.util.Map.Entry;

public class RequestPath {
    private String path;

    /* Used for javascript only */
    private JavascriptModuleType moduleType;

    /* Used for javascript only */
    private boolean defer = false;

    protected RequestPath() {
    }

    public RequestPath(final String path) {
        Preconditions.checkNotNull(path);
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof RequestPath)) {
            return false;
        }

        RequestPath that = (RequestPath) obj;

        return Objects.equal(this.path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(path);
    }

    @Override
    public String toString() {
        return path;
    }

    public boolean isRemote() {
        return path.contains("://") ||
                path.startsWith("//") ||
                path.startsWith("/redirect/") ||
                path.startsWith("/webjars/") ||
                path.startsWith("/webjarsjs");
    }

    public String dirname() {

        int index = path.lastIndexOf("/");
        if (index > -1) {
            return path.substring(0, index);
        }

        return "";
    }

    private boolean isAbsolute() {
        return path.startsWith("/");
    }

    public boolean isRelative() {
        return !isRemote() && !isAbsolute() && !path.startsWith("data:");
    }

    public <T extends RequestPath> T resolvePlaceholders(final Map<String, String> replace) {
        for (Entry<String, String> entry : replace.entrySet()) {
            path = path.replace(entry.getKey(), entry.getValue());
        }

        return (T) this;
    }

    public JavascriptModuleType getModuleType() {
        return moduleType;
    }

    public void setModuleType(JavascriptModuleType moduleType) {
        this.moduleType = moduleType;
    }

    public boolean isDefer() {
        return defer;
    }

    public void setDefer(boolean defer) {
        this.defer = defer;
    }

    public String formatModuleAttribute() {
        if (JavascriptModuleType.nomodule.equals(moduleType)) {
            return "nomodule ";
        }

        if (JavascriptModuleType.module.equals(moduleType)) {
            return "type=\"module\" ";
        }

        return "";
    }

    public String formatDeferAttribute() {
        if (defer) {
            return "defer ";
        }

        return "";
    }
}
