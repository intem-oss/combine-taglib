package se.intem.web.taglib.combined.configuration;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import se.intem.web.taglib.combined.javascript.JavascriptModuleType;
import se.intem.web.taglib.combined.tree.ConfigurationItem;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class CombineJsonConfigurationTest {

    private CombineJsonConfiguration loader;

    @Before
    public void setup() {
        this.loader = new CombineJsonConfiguration("/combine-test.json");
    }

    @Test
    public void should_find_configuration_on_classpath() {
        Optional<ConfigurationItemsCollection> configuration = loader.readConfiguration();
        assertTrue(configuration.isPresent());
        assertEquals(4, configuration.get().size());
    }

    @Test
    public void should_reuse_unmodified_configuration() {
        Optional<ConfigurationItemsCollection> first = loader.readConfiguration();
        assertTrue(first.isPresent());

        Optional<ConfigurationItemsCollection> second = loader.readConfiguration();

        assertSame(first, second);
    }

    @Test
    public void should_find_conditional() {
        Optional<ConfigurationItemsCollection> configuration = loader.readConfiguration();

        List<ConfigurationItem> items = Lists.newArrayList(configuration.get().iterator());
        ConfigurationItem third = items.get(2);
        assertEquals("IE lt 10", third.getConditional());
    }

    @Test
    public void should_parse_javascript_module() {
        Optional<ConfigurationItemsCollection> configuration = loader.readConfiguration();

        List<ConfigurationItem> items = Lists.newArrayList(configuration.get().iterator());
        ConfigurationItem item = items.get(3);
        assertEquals("moduletest", item.getName());
        assertEquals(JavascriptModuleType.nomodule, item.getJs().get(0).getModuleType());
        assertEquals(JavascriptModuleType.module, item.getJs().get(1).getModuleType());
        assertNull(item.getJs().get(2).getModuleType());

    }

    @Test
    public void should_parse_javascript_defer() {
        Optional<ConfigurationItemsCollection> configuration = loader.readConfiguration();

        List<ConfigurationItem> items = Lists.newArrayList(configuration.get().iterator());
        ConfigurationItem item = items.get(3);
        assertEquals("moduletest", item.getName());

        assertTrue(item.getJs().get(0).isDefer());
        assertFalse(item.getJs().get(1).isDefer());
    }

}
